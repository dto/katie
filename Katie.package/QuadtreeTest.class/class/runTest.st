as yet unclassified
runTest
	| q rand sweep background allMorphs |
	MyQuadtree := Quadtree
				createWithBounds: (0 @ 0 corner: 1000 @ 1000)
				toDepth: 4.
	q := MyQuadtree.
	self assert: q isLeaf not.
	0
		to: 16
		do: [:x | 
			rand := 500 random @ 500 random extent: 500 random @ 500 random.
			self
				assert: ((q search: rand) bounds containsRect: rand)].
	sweep := Adventure create: DarkKnight.
	background := AdventureBuffer new.
	background quadtree: q.
	sweep position: 0 @ 0.
	background addMorph: sweep.
	q insert: sweep.
	0
		to: 16
		do: [:x | 
			| m |
			rand := 500 random @ 500 random extent: 499 random @ 499 random.
			m := DeciduousTree new.
			m bounds: rand.
			background addMorph: m.
			q insert: m].
	0
		to: 64
		do: [:x | 
			sweep move: 10 @ 10.
			self
				assert: ((q search: sweep relativeBounds) bounds containsRect: sweep relativeBounds).
			self assert: (q search: sweep relativeBounds)
					= sweep quadtree].
	q deepValidate.
	Transcript cr.
	Transcript show: '*****'.
	allMorphs := OrderedCollection new.
	q
		processRectangle: q bounds
		do: [:node | node morphs
				ifNotNil: [node morphs
						do: [:each | allMorphs add: each]]].
	Transcript show: allMorphs asString