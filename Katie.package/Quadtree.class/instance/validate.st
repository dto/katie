testing
validate
	self isLeaf
		ifTrue: [^ true] 
		ifFalse: [self assert: (self bounds containsRect: (self northEast bounds)).
				self assert: (self bounds containsRect: (self northWest bounds)).
				self assert: (self bounds containsRect: (self southEast bounds)).
				self assert: (self bounds containsRect: (self southWest bounds)).
			      self assert: (self northEast validate).
				self assert: (self northWest validate).
				self assert: (self southEast validate).
				self assert: (self southWest validate).
					^ true]