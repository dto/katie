updating
addMorph: aMorph 
	| result | 
	self ensureMorphsCollection.
	result := (morphs contains: [:elt | elt = aMorph]).
	result ifFalse: [morphs add: aMorph].
	result ifTrue: [ self error: 'redundant addMorph' ].
	aMorph quadtree: self