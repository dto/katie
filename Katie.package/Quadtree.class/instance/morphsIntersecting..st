collision detection
morphsIntersecting: morph 
	| results rect |
	results := OrderedCollection new.
	rect := morph relativeBounds.
	self
		processRectangle: rect
		do: [:node | node morphs
				ifNotNil: [node morphs
						do: [:each | each == morph
								ifFalse: [(rect intersects: each relativeBounds)
										ifTrue: [results add: each]]]]].
	^ results