motion
relativeBounds
	| rect |
	rect := self ownerThatIsA: AdventureBuffer.
	^ Rectangle origin: ((self bounds origin x asFloat) @ (self bounds origin y asFloat)) - ((rect bounds origin x asFloat) @ (rect bounds origin y asFloat)) 
		extent: (self bounds width) @ (self bounds height)