events
myHandleEvent: evt
	| value | 
	value := evt keyString.
	(value = '<up>') ifTrue: [ self move: (0 @ -2) ]. 
	(value = '<down>') ifTrue: [ self move: (0 @ 2) ]. 
	(value = '<left>') ifTrue: [ self move: (-2 @ 0) ]. 
	(value = '<right>') ifTrue: [ self move: (2 @ 0) ]. 
	self comeToFront.
	^ value.