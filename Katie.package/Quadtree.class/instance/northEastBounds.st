accessing
northEastBounds
	| rect nw |
	rect := self bounds.
	nw := Rectangle origin: (((rect left + (rect width / 2)) asFloat) @ (rect top asFloat))
					extent: ((rect extent x / 2) asFloat @ (rect extent y / 2) asFloat).
	^ nw