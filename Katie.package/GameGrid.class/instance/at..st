accessing
at: aPoint
	^ cells at: ((width * (aPoint y)) + (aPoint x) + 1)