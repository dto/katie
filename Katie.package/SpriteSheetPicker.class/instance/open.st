as yet unclassified
open
	| window |
	self ui myLabel color: Color white.
	self ui myLabel contents: 'Click any sprite to see its cell coordinates!'.
	self ui spriteSheet setNewImageFrom: Adventure mySpriteSheet form.
	window := self openInWindowLabeled: 'Sprite Sheet Picker'.
	self ui spriteSheet respondsTo: #mouseMove.
	self ui spriteSheet
		on: #mouseDown
		send: #updateLabel:
		to: self