collision detection
morphsIntersectingRectangle: rect 
	| results |
	results := OrderedCollection new.
	self
		processRectangle: rect
		do: [:node | node morphs
				ifNotNil: [node morphs
						do: [:each | (rect intersects: each relativeBounds)
								ifTrue: [results add: each]]]].
	"Transcript show: results asString."
	^ results