accessing
cellAt: aPoint withColor: aColor
	^ self cellAtX: aPoint x Y: aPoint y withColor: aColor