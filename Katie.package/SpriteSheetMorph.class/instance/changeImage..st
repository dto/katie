looks
changeImage: aSelector 
	| ss |
	imageSelector := aSelector.
	ss := self spriteSheet.
	self form: (ss
				cachedCellAt: (ss positions at: imageSelector)
				withColor: defaultColor)