traversal
processQuadrants: rect do: block 
	self northEast processRectangle: rect do: block.
	self northWest processRectangle: rect do: block.
	self southEast processRectangle: rect do: block.
	self southWest processRectangle: rect do: block.
	^ nil