motion
canMove: aPoint 
	| rect int q |
	rect := self relativeBoundsTransformedBy: aPoint.
	q := self findQuadtreeRoot.
	"don't allow self to leave quadtree bounds"
	(q bounds containsRect: rect)
		ifFalse: [^ false].
	"don't allow self to move if any obstacles intersect desired position"
	int := q morphsIntersectingRectangle: rect.
	^ int
		noneSatisfy: [:each | each ~~ self
				and: [each isObstacle]]