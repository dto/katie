traversal
processRectangle: rect do: block 
	(self bounds intersects: rect)
		ifTrue: [self isLeaf
				ifFalse: [self processQuadrants: rect do: block].
			^ block value: self]