accessing
cellAt: aPoint
	^ self cellAtX: aPoint x Y: aPoint y.