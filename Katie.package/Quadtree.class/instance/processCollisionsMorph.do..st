collision detection
processCollisionsMorph: morph do: block 
	| colliders |
	colliders := self morphsIntersectingRectangle: morph relativeBounds.
	colliders
		ifNotEmpty: [colliders
				do: [:each | each == morph
						ifFalse: [block value: each]]]