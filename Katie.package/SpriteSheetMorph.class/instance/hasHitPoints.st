health
hasHitPoints
	^ hitPoints ifNil: [ ^ false ] ifNotNil: [ ^ true ]