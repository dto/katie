creating
createWithBounds: aRectangle toDepth: level 
	| quadtree |
	quadtree := Quadtree new.
	quadtree bounds: aRectangle.
	level = 0
		ifTrue: [^ quadtree].
	quadtree
		northWest: (self createWithBounds: quadtree northWestBounds toDepth: level - 1);
		southWest: (self createWithBounds: quadtree southWestBounds toDepth: level - 1);
		northEast: (self createWithBounds: quadtree northEastBounds toDepth: level - 1);
		southEast: (self createWithBounds: quadtree southEastBounds toDepth: level - 1).
	^ quadtree