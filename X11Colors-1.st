 "rgb.lisp --- X11 color data for XELF

 This file has been reformatted from its original version so as to
 be directly readable by Common Lisp, and is under the MIT
 License. The license and original copyright notice are reprinted
 below.

 Copyright (C) 1994 X Consortium

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the 'Software'), to deal in the Software without
 restriction, including without limitation the rights to use, copy,
 modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT.  IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR
 ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 Except as contained in this notice, the name of the X Consortium
 shall not be used in advertising or otherwise to promote the sale,
 use or other dealings in this Software without prior written
 authorization from the X Consortium.

 X Window System is a trademark of X Consortium, Inc."

| temp |
temp := X11Color colorNames.
{'AliceBlue' -> #(240 248 255) .
'AntiqueWhite' -> #(250 235 215) .
'AntiqueWhite1' -> #(255 239 219) .
'AntiqueWhite2' -> #(238 223 204) .
'AntiqueWhite3' -> #(205 192 176) .
'AntiqueWhite4' -> #(139 131 120) .
'BlanchedAlmond' -> #(255 235 205) .
'BlueViolet' -> #(138 43 226) .
'CadetBlue' -> #(95 158 160) .
'CadetBlue1' -> #(152 245 255) .
'CadetBlue2' -> #(142 229 238) .
'CadetBlue3' -> #(122 197 205) .
'CadetBlue4' -> #(83 134 139) .
'CornflowerBlue' -> #(100 149 237) .
'DarkBlue' -> #(0 0 139) .
'DarkCyan' -> #(0 139 139) .
'DarkGoldenrod' -> #(184 134 11) .
'DarkGoldenrod1' -> #(255 185 15) .
'DarkGoldenrod2' -> #(238 173 14) .
'DarkGoldenrod3' -> #(205 149 12) .
'DarkGoldenrod4' -> #(139 101 8) .
'DarkGray' -> #(169 169 169) .
'DarkGreen' -> #(0 100 0) .
'DarkGrey' -> #(169 169 169) .
'DarkKhaki' -> #(189 183 107) .
'DarkMagenta' -> #(139 0 139) .
'DarkOliveGreen' -> #(85 107 47) .
'DarkOliveGreen1' -> #(202 255 112) .
'DarkOliveGreen2' -> #(188 238 104) .
'DarkOliveGreen3' -> #(162 205 90) .
'DarkOliveGreen4' -> #(110 139 61) .
'DarkOrange' -> #(255 140 0) .
'DarkOrange1' -> #(255 127 0) .
'DarkOrange2' -> #(238 118 0) .
'DarkOrange3' -> #(205 102 0) .
'DarkOrange4' -> #(139 69 0) .
'DarkOrchid' -> #(153 50 204) .
'DarkOrchid1' -> #(191 62 255) .
'DarkOrchid2' -> #(178 58 238) .
'DarkOrchid3' -> #(154 50 205) .
'DarkOrchid4' -> #(104 34 139) .
'DarkRed' -> #(139 0 0) .
'DarkSalmon' -> #(233 150 122) .
'DarkSeaGreen' -> #(143 188 143) .
'DarkSeaGreen1' -> #(193 255 193) .
'DarkSeaGreen2' -> #(180 238 180) .
'DarkSeaGreen3' -> #(155 205 155) .
'DarkSeaGreen4' -> #(105 139 105) .
'DarkSlateBlue' -> #(72 61 139) .
'DarkSlateGray' -> #(47 79 79) .
'DarkSlateGray1' -> #(151 255 255) .
'DarkSlateGray2' -> #(141 238 238) .
'DarkSlateGray3' -> #(121 205 205) .
'DarkSlateGray4' -> #(82 139 139) .
'DarkSlateGrey' -> #(47 79 79) .
'DarkTurquoise' -> #(0 206 209) .
'DarkViolet' -> #(148 0 211) .
'DeepPink' -> #(255 20 147) .
'DeepPink1' -> #(255 20 147) .
'DeepPink2' -> #(238 18 137) .
'DeepPink3' -> #(205 16 118) .
'DeepPink4' -> #(139 10 80) .
'DeepSkyBlue' -> #(0 191 255) .
'DeepSkyBlue1' -> #(0 191 255) .
'DeepSkyBlue2' -> #(0 178 238) .
'DeepSkyBlue3' -> #(0 154 205) .
'DeepSkyBlue4' -> #(0 104 139) .
'DimGray' -> #(105 105 105) .
'DimGrey' -> #(105 105 105) .
'DodgerBlue' -> #(30 144 255) .
'DodgerBlue1' -> #(30 144 255) .
'DodgerBlue2' -> #(28 134 238) }
    do: [ :pair | temp add: pair ].
    

