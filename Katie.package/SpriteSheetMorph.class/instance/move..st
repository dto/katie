motion
move: aPoint 
	(self canMove: aPoint)
		ifTrue: [self position: self position + aPoint.
				self updateQuadtree: self findQuadtreeRoot]