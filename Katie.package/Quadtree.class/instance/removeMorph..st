updating
removeMorph: aMorph 
	self
		assert: (morphs
				contains: [:x | x = aMorph]).
	morphs remove: aMorph