collision detection
processCollisions: rect do: block
	| colliders | 
	colliders := self morphsIntersectingRectangle: rect.
	colliders ifNotEmpty: [colliders do: [ :each | block value: each ]].