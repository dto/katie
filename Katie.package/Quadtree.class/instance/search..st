search
search: rect 
	"find the smallest quadtree node containing the argument rectangle, or nil if none is found."
	| found |
	(self bounds containsRect: rect)
		ifTrue: [self isLeaf
				ifFalse: [found := self searchQuadrants: rect].
			found
				ifNil: [^ self]
				ifNotNil: [^ found]]
		ifFalse: [^ nil]