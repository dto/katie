| temp |
temp := X11Color colorNames.
{
'grey62' -> #(158 158 158) .
'grey63' -> #(161 161 161) .
'grey64' -> #(163 163 163) .
'grey65' -> #(166 166 166) .
'grey66' -> #(168 168 168) .
'grey67' -> #(171 171 171) .
'grey68' -> #(173 173 173) .
'grey69' -> #(176 176 176) .
'grey7' -> #(18 18 18) .
'grey70' -> #(179 179 179) .
'grey71' -> #(181 181 181) .
'grey72' -> #(184 184 184) .
'grey73' -> #(186 186 186) .
'grey74' -> #(189 189 189) .
'grey75' -> #(191 191 191) .
'grey76' -> #(194 194 194) .
'grey77' -> #(196 196 196) .
'grey78' -> #(199 199 199) .
'grey79' -> #(201 201 201) .
'grey8' -> #(20 20 20) .
'grey80' -> #(204 204 204) .
'grey81' -> #(207 207 207) .
'grey82' -> #(209 209 209) .
'grey83' -> #(212 212 212) .
'grey84' -> #(214 214 214) .
'grey85' -> #(217 217 217) .
'grey86' -> #(219 219 219) .
'grey87' -> #(222 222 222) .
'grey88' -> #(224 224 224) .
'grey89' -> #(227 227 227) .
'grey9' -> #(23 23 23) .
'grey90' -> #(229 229 229) .
'grey91' -> #(232 232 232) .
'grey92' -> #(235 235 235) .
'grey93' -> #(237 237 237) .
'grey94' -> #(240 240 240) .
'grey95' -> #(242 242 242) .
'grey96' -> #(245 245 245) .
'grey97' -> #(247 247 247) .
'grey98' -> #(250 250 250) .
'grey99' -> #(252 252 252) .
'honeydew' -> #(240 255 240) .
'honeydew1' -> #(240 255 240) .
'honeydew2' -> #(224 238 224) .
'honeydew3' -> #(193 205 193) .
'honeydew4' -> #(131 139 131) .
'hot pink' -> #(255 105 180) .
'indian red' -> #(205 92 92) .
'ivory' -> #(255 255 240) .
'ivory1' -> #(255 255 240) .
'ivory2' -> #(238 238 224) .
'ivory3' -> #(205 205 193) .
'ivory4' -> #(139 139 131) .
'khaki' -> #(240 230 140) .
'khaki1' -> #(255 246 143) .
'khaki2' -> #(238 230 133) .
'khaki3' -> #(205 198 115) .
'khaki4' -> #(139 134 78) .
'lavender blush' -> #(255 240 245) .
'lavender' -> #(230 230 250) .
'lawn green' -> #(124 252 0) .
'lemon chiffon' -> #(255 250 205) .
'light blue' -> #(173 216 230) .
'light coral' -> #(240 128 128) .
'light cyan' -> #(224 255 255) .
'light goldenrod yellow' -> #(250 250 210) .
'light goldenrod' -> #(238 221 130) .
'light gray' -> #(211 211 211) .
'light green' -> #(144 238 144) .
'light grey' -> #(211 211 211) .
'light pink' -> #(255 182 193) .
'light salmon' -> #(255 160 122) .
'light sea green' -> #(32 178 170) .
'light sky blue' -> #(135 206 250) .
'light slate blue' -> #(132 112 255) .
'light slate gray' -> #(119 136 153) .
'light slate grey' -> #(119 136 153) .
'light steel blue' -> #(176 196 222) .
'light yellow' -> #(255 255 224) .
'lime green' -> #(50 205 50) .
'linen' -> #(250 240 230) .
'magenta' -> #(255 0 255) .
'magenta1' -> #(255 0 255) .
'magenta2' -> #(238 0 238) .
'magenta3' -> #(205 0 205) .
'magenta4' -> #(139 0 139) .
'maroon' -> #(176 48 96) .
'maroon1' -> #(255 52 179) .
'maroon2' -> #(238 48 167) .
'maroon3' -> #(205 41 144) .
'maroon4' -> #(139 28 98) .
'medium aquamarine' -> #(102 205 170) .
'medium blue' -> #(0 0 205) .
'medium orchid' -> #(186 85 211) .
'medium purple' -> #(147 112 219) .
'medium sea green' -> #(60 179 113) .
'medium slate blue' -> #(123 104 238) .
'medium spring green' -> #(0 250 154) .
'medium turquoise' -> #(72 209 204) .
'medium violet red' -> #(199 21 133) .
'midnight blue' -> #(25 25 112) .
'mint cream' -> #(245 255 250) .
'misty rose' -> #(255 228 225) .
'moccasin' -> #(255 228 181) .
'navajo white' -> #(255 222 173) .
'navy blue' -> #(0 0 128) .
'navy' -> #(0 0 128) .
'old lace' -> #(253 245 230) .
'olive drab' -> #(107 142 35) .
'orange red' -> #(255 69 0) .
'orange' -> #(255 165 0) .
'orange1' -> #(255 165 0) .
'orange2' -> #(238 154 0) 
 }
    do: [ :pair | temp add: pair ].
