accessing
cellAtX: x Y: y 
	| newForm |
	newForm := Form basicNew setExtent: cellWidth @ cellHeight depth: 32.
	newForm
		copyBits: (x * cellWidth @ (y * cellHeight) corner: x + 1 * cellWidth @ (y + 1 * cellHeight))
		from: form
		at: 0 @ 0
		clippingBox: newForm boundingBox
		rule: Form over
		fillColor: defaultColor.
	^ newForm