creating
exportBuffer
	| morph xSize ySize |
	morph := AdventureBuffer new.
	morph color: Color black.
	morph position: 0 @ 0.
	morph layoutChanged; fullBounds.
	(0 to: width - 1)
		do: [:x | (0 to: height - 1)
				do: [:y | 
					| c |
					c := self at: x @ y.
					c
						ifNotNil: [c
								do: [:each | 
									morph addMorphFront: each.
									xSize := each width.
									ySize := each height.
									each position: x * xSize @ (y * ySize)]]]].
	morph extent: xSize * width @ (ySize * height).
	morph dropEnabled: true.
	morph
		quadtree: (Quadtree
				createWithBounds: (0 @ 0 corner: morph extent)
				toDepth: 4).
	morph layoutChanged; fullBounds.
	morph quadtree fillWithMorphs: morph submorphs.
	^ morph