initialization
initialize
	super initialize.
	cellHeight := 16.
	cellWidth := 16.
	defaultColor := Color white.
	defaultScale := 2.
	"load DENZI graphics now"
	self form: (Form fromFileNamed: (self class denziSpriteSheetFile)).
