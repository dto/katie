accessing
cachedCellAt: aPoint withColor: aColor
	^ self cachedCellAtX: aPoint x Y: aPoint y withColor: aColor