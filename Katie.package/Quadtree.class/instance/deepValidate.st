testing
deepValidate
	self validate.
	"check that current node contains every morph"
	self morphs ifNotNil: [ self morphs do: [ :each | self assert: (self bounds containsRect: (each relativeBounds))]].
	"check that current node does not have any morphs that should be in child nodes"
	self isLeaf ifFalse: [self morphs ifNotNil: [self morphs do: [ :each | self assert: ((self northEast bounds containsRect: (each relativeBounds)) not).
												 self assert: ((self northWest bounds containsRect: (each relativeBounds)) not).
												 self assert: ((self southEast bounds containsRect: (each relativeBounds)) not).
												 self assert: ((self southWest bounds containsRect: (each relativeBounds)) not)]]].
	"check child nodes"
	self isLeaf ifFalse: [self northEast deepValidate.
					self northWest deepValidate.
					self southEast deepValidate.
					self southWest deepValidate].
	^ true
