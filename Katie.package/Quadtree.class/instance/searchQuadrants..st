search
searchQuadrants: rect 
	| found |
	found := self northEast search: rect.
	found
		ifNotNil: [^ found].
	found := self southEast search: rect.
	found
		ifNotNil: [^ found].
	found := self northWest search: rect.
	found
		ifNotNil: [^ found].
	found := self southWest search: rect.
	found
		ifNotNil: [^ found].
	^ nil