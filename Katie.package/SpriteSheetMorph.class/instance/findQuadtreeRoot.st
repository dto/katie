motion
findQuadtreeRoot
	| m |
	m := self ownerThatIsA: AdventureBuffer.
	m
		ifNil: [^ nil]
		ifNotNil: [^ m quadtree]