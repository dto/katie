 "rgb.lisp --- X11 color data for XELF

 This file has been reformatted from its original version so as to
 be directly readable by Common Lisp, and is under the MIT
 License. The license and original copyright notice are reprinted
 below.

 Copyright (C) 1994 X Consortium

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the 'Software'), to deal in the Software without
 restriction, including without limitation the rights to use, copy,
 modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT.  IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR
 ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNEC-
 TION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 Except as contained in this notice, the name of the X Consortium
 shall not be used in advertising or otherwise to promote the sale,
 use or other dealings in this Software without prior written
 authorization from the X Consortium.

 X Window System is a trademark of X Consortium, Inc."

| temp |
temp := X11Color colorNames.
{
'MediumSlateBlue' -> #(123 104 238) .
'MediumSpringGreen' -> #(0 250 154) .
'MediumTurquoise' -> #(72 209 204) .
'MediumVioletRed' -> #(199 21 133) .
'MidnightBlue' -> #(25 25 112) .
'MintCream' -> #(245 255 250) .
'MistyRose' -> #(255 228 225) .
'MistyRose1' -> #(255 228 225) .
'MistyRose2' -> #(238 213 210) .
'MistyRose3' -> #(205 183 181) .
'MistyRose4' -> #(139 125 123) .
'NavajoWhite' -> #(255 222 173) .
'NavajoWhite1' -> #(255 222 173) .
'NavajoWhite2' -> #(238 207 161) .
'NavajoWhite3' -> #(205 179 139) .
'NavajoWhite4' -> #(139 121 94) .
'NavyBlue' -> #(0 0 128) .
'OldLace' -> #(253 245 230) .
'OliveDrab' -> #(107 142 35) .
'OliveDrab1' -> #(192 255 62) .
'OliveDrab2' -> #(179 238 58) .
'OliveDrab3' -> #(154 205 50) .
'OliveDrab4' -> #(105 139 34) .
'OrangeRed' -> #(255 69 0) .
'OrangeRed1' -> #(255 69 0) .
'OrangeRed2' -> #(238 64 0) .
'OrangeRed3' -> #(205 55 0) .
'OrangeRed4' -> #(139 37 0) .
'PaleGoldenrod' -> #(238 232 170) .
'PaleGreen' -> #(152 251 152) .
'PaleGreen1' -> #(154 255 154) .
'PaleGreen2' -> #(144 238 144) .
'PaleGreen3' -> #(124 205 124) .
'PaleGreen4' -> #(84 139 84) .
'PaleTurquoise' -> #(175 238 238) .
'PaleTurquoise1' -> #(187 255 255) .
'PaleTurquoise2' -> #(174 238 238) .
'PaleTurquoise3' -> #(150 205 205) .
'PaleTurquoise4' -> #(102 139 139) .
'PaleVioletRed' -> #(219 112 147) .
'PaleVioletRed1' -> #(255 130 171) .
'PaleVioletRed2' -> #(238 121 159) .
'PaleVioletRed3' -> #(205 104 137) .
'PaleVioletRed4' -> #(139 71 93) .
'PapayaWhip' -> #(255 239 213) .
'PeachPuff' -> #(255 218 185) .
'PeachPuff1' -> #(255 218 185) .
'PeachPuff2' -> #(238 203 173) .
'PeachPuff3' -> #(205 175 149) .
'PeachPuff4' -> #(139 119 101) .
'PowderBlue' -> #(176 224 230) .
'RosyBrown' -> #(188 143 143) .
'RosyBrown1' -> #(255 193 193) .
'RosyBrown2' -> #(238 180 180) .
'RosyBrown3' -> #(205 155 155) .
'RosyBrown4' -> #(139 105 105) .
'RoyalBlue' -> #(65 105 225) .
'RoyalBlue1' -> #(72 118 255) .
'RoyalBlue2' -> #(67 110 238) .
'RoyalBlue3' -> #(58 95 205) .
'RoyalBlue4' -> #(39 64 139) .
'SaddleBrown' -> #(139 69 19) .
'SandyBrown' -> #(244 164 96) .
'SeaGreen' -> #(46 139 87) .
'SeaGreen1' -> #(84 255 159) .
'SeaGreen2' -> #(78 238 148) .
'SeaGreen3' -> #(67 205 128) .
'SeaGreen4' -> #(46 139 87) .
'SkyBlue' -> #(135 206 235) .
'SkyBlue1' -> #(135 206 255) .
'SkyBlue2' -> #(126 192 238) .
'SkyBlue3' -> #(108 166 205) .
'SkyBlue4' -> #(74 112 139) .
'SlateBlue' -> #(106 90 205) .
'SlateBlue1' -> #(131 111 255) .
'SlateBlue2' -> #(122 103 238) .
'SlateBlue3' -> #(105 89 205) .
'SlateBlue4' -> #(71 60 139) .
'SlateGray' -> #(112 128 144) .
'SlateGray1' -> #(198 226 255) .
'SlateGray2' -> #(185 211 238) .
'SlateGray3' -> #(159 182 205) .
'SlateGray4' -> #(108 123 139) .
'SlateGrey' -> #(112 128 144) .
'SpringGreen' -> #(0 255 127) .
'SpringGreen1' -> #(0 255 127) .
'SpringGreen2' -> #(0 238 118) .
'SpringGreen3' -> #(0 205 102) 
 }
    do: [ :pair | temp add: pair ].


    

