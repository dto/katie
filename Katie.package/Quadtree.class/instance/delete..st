updating
delete: morph 
	| node |
	node := morph quadtree.
	node ifNotNil: [ node removeMorph: morph.
					morph quadtree: nil]