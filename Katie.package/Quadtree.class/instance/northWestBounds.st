accessing
northWestBounds
	| rect nw |
	rect := self bounds.
	nw := Rectangle origin: (rect origin) extent: ((rect extent x / 2) asFloat @ (rect extent y / 2) asFloat).
	^ nw