accessing
at: aPoint add: anObject
	| objects | 
	objects := (self at: aPoint) ifNil: [self at: aPoint put: OrderedCollection new].
	objects add: anObject.