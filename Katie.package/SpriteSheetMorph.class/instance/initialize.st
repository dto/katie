initialization
initialize 
	super initialize.
	defaultColor ifNil: [ defaultColor := Color white].
	self spriteSheet: Adventure mySpriteSheet.
	self changeImage: imageSelector.
	self width: spriteSheet defaultScale * self width;
		height: spriteSheet defaultScale * self height.
	^ self