accessing
at: aPoint put: aValue
	^ cells at: ((width * (aPoint y)) + (aPoint x) + 1) put: aValue