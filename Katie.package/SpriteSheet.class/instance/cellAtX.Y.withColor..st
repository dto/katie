accessing
cellAtX: x Y: y withColor: aColor
	self defaultColor: aColor.
	^ self cellAtX: x Y: y. 