accessing
cachedCellAtX: x Y: y withColor: aColor
	| key | 
	self defaultColor: aColor.
	key := { x . y . aColor }.
	cache at: key ifAbsent: [ cache at: key put: (self cellAtX: x Y: y withColor: aColor) ].
	^ cache at: key  
	