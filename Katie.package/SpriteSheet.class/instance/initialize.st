initialization
initialize
	self initializePositions.
	self initializeCache.
	cellHeight := 16.
	cellWidth := 16.
	defaultColor := Color white.
	defaultScale := 2.
