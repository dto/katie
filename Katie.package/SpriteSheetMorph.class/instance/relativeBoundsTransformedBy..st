motion
relativeBoundsTransformedBy: aPoint 
	| theBounds |
	theBounds := self relativeBounds.
	^ Rectangle origin: theBounds origin + aPoint extent: theBounds extent