as yet unclassified
runTest
	| grid knight window rect |
	MyAdventure := Adventure new.
	knight := DarkKnight new.
	grid := GameGrid newGrid: 16 @ 16.
	(0 to: 15)
		do: [:x | (0 to: 15)
				do: [:y | 
					grid at: x @ y add: Dirt new.
					(0 to: 3) atRandom = 1
						ifTrue: [grid
								at: x @ y
								add: ({ConiferousTree. DeciduousTree} atRandom new)]]].
	(grid at: 1 @ 1)
		add: knight.
	MyAdventure player1: knight.
	rect := grid exportBuffer.
	window := rect openInWindowLabeled: 'Adventure Test'.
	window
		on: #keyStroke
		send: #myHandleEvent:
		to: knight