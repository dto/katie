as yet unclassified
named: name
	| names in out |
	names := self colorNames.
	names ifEmpty: [ self loadColorDataFiles ].
	names := self colorNames.
	in := names at: name.
	out := Array new: 3.
	"convert from 0...255 format to 0..1"
	out at: 1 put: ((in at: 1) / 256.0).  
	out at: 2 put: ((in at: 2) / 256.0).  
	out at: 3 put: ((in at: 3) / 256.0).  
	^ Color fromArray: out.
