ui - private
initializePanel
	"This code is auto-generated. DO NOT modify it because another generation step will overwrite all changes."
	
	|  |
	
	panel := UiContainer new.
	panel changeTableLayout.
	panel extent: (500@300).
	panel position: (0@0).
	panel hResizing: (#rigid).
	panel vResizing: (#rigid).
	panel minWidth: (1).
	panel minHeight: (1).
	panel cellInset: (0).
	panel cellPositioning: (#center).
	panel cellSpacing: (#none).
	panel layoutInset: (4).
	panel listCentering: (#topLeft).
	panel listDirection: (#topToBottom).
	panel listSpacing: (#none).
	panel maxCellSize: (1073741823).
	panel minCellSize: (0).
	panel wrapCentering: (#topLeft).
	panel wrapDirection: (#none).
	panel frameFractions: (0@0 corner: 1@1).
	panel frameOffsets: (0@0 corner: 0@0).
	panel balloonText: (nil).
	panel balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	panel color: (Color black).
	panel borderWidth: (0).
	panel borderStyle2: (#simple).
	panel borderColor: (Color black).
	panel setProperty: #objectName toValue: 'panel'.
	spriteSheet := ImageMorph new.
	spriteSheet changeTableLayout.
	spriteSheet extent: (80@40).
	spriteSheet position: (4@4).
	spriteSheet hResizing: (#shrinkWrap).
	spriteSheet vResizing: (#shrinkWrap).
	spriteSheet minWidth: (1).
	spriteSheet minHeight: (1).
	spriteSheet cellInset: (0).
	spriteSheet cellPositioning: (#center).
	spriteSheet cellSpacing: (#none).
	spriteSheet layoutInset: (0).
	spriteSheet listCentering: (#topLeft).
	spriteSheet listDirection: (#topToBottom).
	spriteSheet listSpacing: (#none).
	spriteSheet maxCellSize: (1152921504606846975).
	spriteSheet minCellSize: (0).
	spriteSheet wrapCentering: (#topLeft).
	spriteSheet wrapDirection: (#none).
	spriteSheet frameFractions: (0@0 corner: 1@1).
	spriteSheet frameOffsets: (0@0 corner: 0@0).
	spriteSheet balloonText: (nil).
	spriteSheet balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	spriteSheet setProperty: #objectName toValue: 'spriteSheet'.
	panel addChild: spriteSheet.
	myLabel := StringMorph new.
	myLabel extent: (79@14).
	myLabel position: (5@48).
	myLabel hResizing: (#rigid).
	myLabel vResizing: (#rigid).
	myLabel minWidth: (79).
	myLabel minHeight: (14).
	myLabel cellInset: (0).
	myLabel cellPositioning: (#center).
	myLabel cellSpacing: (#none).
	myLabel layoutInset: (0).
	myLabel listCentering: (#topLeft).
	myLabel listDirection: (#topToBottom).
	myLabel listSpacing: (#none).
	myLabel maxCellSize: (1152921504606846975).
	myLabel minCellSize: (0).
	myLabel wrapCentering: (#topLeft).
	myLabel wrapDirection: (#none).
	myLabel frameFractions: (0@0 corner: 1@1).
	myLabel frameOffsets: (0@0 corner: 0@0).
	myLabel balloonText: (nil).
	myLabel balloonColor: ((Color r: 0.934 g: 0.91 b: 0.836)).
	myLabel setProperty: #objectName toValue: 'myLabel'.
	panel addChild: myLabel.
	panel setProperty: #uiClassName toValue: 'SpriteSheetPicker'.